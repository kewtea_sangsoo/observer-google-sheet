var SCRIPT_PROP = PropertiesService.getScriptProperties(); // new property service

// If you don't want to expose either GET or POST methods you can comment out the appropriate function
function doGet(e){
    return handleResponse(e);
}

function doPost(e){
    return handleResponse(e);
}

function handleResponse(e) {
    // shortly after my original solution Google announced the LockService[1]
    // this prevents concurrent access overwritting data
    // [1] http://googleappsdeveloper.blogspot.co.uk/2011/10/concurrency-and-google-apps-script.html
    // we want a public lock, one that locks for all invocations
    var lock = LockService.getPublicLock();
    lock.waitLock(30000);  // wait 30 seconds before conceding defeat.

    try {
        // next set where we write the data - you could write to multiple/alternate destinations
        var doc = SpreadsheetApp.openById("1obltJ3y8i4mHfRj2qz_WSR0gMHPh7ylR3gvbpn0qRwU");
        var sheetName = e.parameter.sheetName;
        var sheet = doc.getSheetByName(sheetName);

        // we'll assume header is in row 1 but you can override with header_row in GET/POST data
        var headRow = 1;
        var headers = sheet.getRange(1, 1, 1, sheet.getLastColumn()).getValues()[0];
        var targetRow = sheet.getLastRow()+1;

        if ("server_data" == sheetName) {
            if ("merge" == e.parameter.method) {
                var paramData = JSON.parse(e.parameter.data);
                var cachedData = paramData;
                var cachedDataKeys = Object.getOwnPropertyNames(cachedData);
                cachedDataKeys.forEach(function(cachedDataKey) {
                    var cachedDataValue = cachedData[cachedDataKey];
                    var column = 1;
                    var columnValues = sheet.getRange(2, column, sheet.getLastRow()).getValues(); //1st is header row
                    var searchResult = columnValues.findIndex(cachedDataKey);
                    if (searchResult != -1) {
                        // 겹치는 데이터
                        // text
                        var columnNum = 2;
                        var targetRow = sheet.getRange(searchResult+2, columnNum, 1, 1);
                        targetRow.setValue(cachedDataValue.text);
                        // last modified time
                        columnNum = 3;
                        targetRow = sheet.getRange(searchResult+2, columnNum, 1, 1);
                        targetRow.setValue(cachedDataValue.lastmodifiedtime);
                    } else {
                        // 새 데이터
                        var targetRow = sheet.getLastRow()+1;
                        var headRow = 1;
                        var headers = sheet.getRange(1, 1, 1, sheet.getLastColumn()).getValues()[0];
                        var row = [];
                        for (i in headers) {
                            if (headers[i] == "id") row.push(cachedDataKey);
                            if (headers[i] == "text") row.push(cachedDataValue.text);
                            if (headers[i] == "lastmodifiedtime") row.push(cachedDataValue.lastmodifiedtime);
                        }
                        sheet.getRange(targetRow, 1, 1, row.length).setValues([row]);
                    }
                });
                return ContentService
                    .createTextOutput(JSON.stringify({"result":"success"}))
                    .setMimeType(ContentService.MimeType.JSON);
            }
        }

        if ("users" == sheetName) {
            if ("signin" == e.parameter.method) {
                var paramData = JSON.parse(e.parameter.data);
                var userEmail = paramData.email;
                var column = 1;
                var columnValues = sheet.getRange(2, column, sheet.getLastRow()).getValues(); //1st is header row
                var searchResult = columnValues.findIndex(userEmail);
                if (searchResult != -1) {
                    return ContentService
                        .createTextOutput(JSON.stringify({"result":"success"}))
                        .setMimeType(ContentService.MimeType.JSON);
                } else {
                    return ContentService
                        .createTextOutput(JSON.stringify({"result":"error", "error": "user not found"}))
                        .setMimeType(ContentService.MimeType.JSON);
                }
            }
            if ("signup" == e.parameter.method) {
                var paramData = JSON.parse(e.parameter.data);
                var userEmail = paramData.email;
                var column = 1;
                var columnValues = sheet.getRange(2, column, sheet.getLastRow()).getValues(); //1st is header row
                var searchResult = columnValues.findIndex(userEmail);
                if (searchResult != -1) {
                    return ContentService
                        .createTextOutput(JSON.stringify({"result":"error", "error": "already exist user"}))
                        .setMimeType(ContentService.MimeType.JSON);
                } else {
                    var headRow = 1;
                    var headers = sheet.getRange(1, 1, 1, sheet.getLastColumn()).getValues()[0];
                    var row = [];
                    for (i in headers) {
                        if (headers[i] == "email") row.push(userEmail);
                        if (headers[i] == "pw") row.push(paramData.pw);
                        if (headers[i] == "validate") row.push(false);
                    }
                    sheet.getRange(targetRow, 1, 1, row.length).setValues([row]);

                    return ContentService
                        .createTextOutput(JSON.stringify({"result":"success"}))
                        .setMimeType(ContentService.MimeType.JSON);
                }
            }
        }


        if ("urls" == sheetName) {
            var paramKey = e.parameter.key;
            var column = 1;
            var columnValues = sheet.getRange(2, column, sheet.getLastRow()).getValues(); //1st is header row
            var searchResult = columnValues.findIndex(paramKey);
            if (searchResult != -1) {

                var row = [];
                // loop through the header columns

                for (i in headers) {
                    if (headers[i] == "lastModifiedTime"){ // special case if you include a 'Timestamp' column
                        row.push(new Date().getTime());
                    } else { // else use header name to get data
                        row.push(e.parameter[headers[i]]);
                    }
                }

                if ("addCommentIds" == e.parameter.method) {
                    var columnNum = 2;
                    var targetRow = sheet.getRange(searchResult+2, columnNum, 1, 1);
                    var commentIds = JSON.parse(targetRow.getValue());
                    var paramIds = JSON.parse(e.parameter.commentIds);
                    for (var i=0; i<paramIds.length; i++) {
                        commentIds.push(paramIds[i]);
                    }
                    targetRow.setValue(JSON.stringify(commentIds));

                    return ContentService
                        .createTextOutput(JSON.stringify({"result":"success", "returnRow":targetRow.getValue()}))
                        .setMimeType(ContentService.MimeType.JSON);
                }

            } else {
                return ContentService
                    .createTextOutput(JSON.stringify({"result":"error", "error": "id not found"}))
                    .setMimeType(ContentService.MimeType.JSON);
            }
        }

        if ("comments" == sheetName) {

            if ("editComment" == e.parameter.method) {
                var paramKey = e.parameter.key;
                var column = 1;
                var columnValues = sheet.getRange(2, column, sheet.getLastRow()).getValues(); //1st is header row
                var searchResult = columnValues.findIndex(paramKey);

                var newText = JSON.parse(e.parameter.data).text;
                var columnNum = 2;
                var targetRow = sheet.getRange(searchResult+2, columnNum, 1, 1);
                targetRow.setValue(newText);

                columnNum = 4;
                targetRow = sheet.getRange(searchResult+2, columnNum, 1, 1);
                targetRow.setValue(new Date().getTime());

                return ContentService
                    .createTextOutput(JSON.stringify({"result":"success", "test":targetRow }))
                    .setMimeType(ContentService.MimeType.JSON);

            }

            if ("deleteComment" == e.parameter.method) {
                // 이상한 key 넣어도 동작하는거같음. 테스트필요
                var paramKey = e.parameter.key;
                var column = 1;
                var columnValues = sheet.getRange(2, column, sheet.getLastRow()).getValues(); //1st is header row
                var searchResult = columnValues.findIndex(paramKey);

                var columnNum = 3;
                var targetRow = sheet.getRange(searchResult+2, columnNum, 1, 1);

                var urlKey = "url_"+targetRow.getValue();

                sheet.deleteRow(searchResult+2);

                // delete from urls table
                sheet = doc.getSheetByName("urls");
                column = 1;
                columnValues = sheet.getRange(2, column, sheet.getLastRow()).getValues(); //1st is header row
                searchResult = columnValues.findIndex(urlKey);

                columnNum = 2;
                targetRow = sheet.getRange(searchResult+2, columnNum, 1, 1);
                var targetId = paramKey.substr(8);
                var commentIds = JSON.parse(targetRow.getValue());
                commentIds.remove(targetId);
                targetRow.setValue(JSON.stringify(commentIds));

                columnNum = 3;
                targetRow = sheet.getRange(searchResult+2, columnNum, 1, 1);
                targetRow.setValue(new Date().getTime());


                return ContentService
                    .createTextOutput(JSON.stringify({"result":"success"}))
                    .setMimeType(ContentService.MimeType.JSON);
            }

            if ("addComment" == e.parameter.method) {
                var targetRow = sheet.getLastRow()+1;

                // 데이터 작성 ... id 생성 ...
                var newId = generateId();
                var headRow = 1;
                var headers = sheet.getRange(1, 1, 1, sheet.getLastColumn()).getValues()[0];
                var row = [];
                var paramData = JSON.parse(e.parameter.data);
                for (i in headers) {
                    if (headers[i] == "key") row.push("comment_"+newId);
                    if (headers[i] == "lastModifiedTime") row.push(new Date().getTime());
                    if (headers[i] == "text") row.push(paramData.text);
                    if (headers[i] == "originURL") {
                        row.push(paramData.originURL);
                    }
                }
                sheet.getRange(targetRow, 1, 1, row.length).setValues([row]);

                // urls sheet 의 commentIds 에도 추가 필요
                var paramKey = paramData.originURL;
                var column = 1;
                sheet = doc.getSheetByName("urls");
                var columnValues = sheet.getRange(2, column, sheet.getLastRow()).getValues(); //1st is header row
                var searchResult = columnValues.findIndex("url_"+paramKey);

                var columnNum = 2;
                var targetRow = sheet.getRange(searchResult+2, columnNum, 1, 1);
                var commentIds = JSON.parse(targetRow.getValue());
                commentIds.push(newId);
                targetRow.setValue(JSON.stringify(commentIds));

                var columnNum = 3;
                var targetRow = sheet.getRange(searchResult+2, columnNum, 1, 1);
                targetRow.setValue(new Date().getTime());

                return ContentService
                    .createTextOutput(JSON.stringify({"result":"success", "test": paramData }))
                    .setMimeType(ContentService.MimeType.JSON);
            }

        }

    } catch(e){
        // if error return this
        return ContentService
            .createTextOutput(JSON.stringify({"result":"error", "error": e}))
            .setMimeType(ContentService.MimeType.JSON);
    } finally { //release lock
        lock.releaseLock();
    }

}

function setup() {
    var doc = SpreadsheetApp.getActiveSpreadsheet();
    SCRIPT_PROP.setProperty("key", doc.getId());
}

Array.prototype.findIndex = function(search){
    if(search == "") return false;
    for (var i=0; i<this.length; i++)
        if (this[i] == search) return i;

    return -1;
}

Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

function generateId() {
    var timeStamp = new Date().getTime().toString();
    for (var i=0; i<10; i++) timeStamp += Math.floor((Math.random() * 10) + 1);
    return timeStamp;
}