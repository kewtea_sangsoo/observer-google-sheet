var SCRIPT_PROP = PropertiesService.getScriptProperties(); // new property service

// If you don't want to expose either GET or POST methods you can comment out the appropriate function
function doGet(e){ return handleResponse(e); }
function doPost(e){ return handleResponse(e); }
function handleResponse(e) {
    // shortly after my original solution Google announced the LockService[1]
    // this prevents concurrent access overwritting data
    // [1] http://googleappsdeveloper.blogspot.co.uk/2011/10/concurrency-and-google-apps-script.html
    // we want a public lock, one that locks for all invocations
    var lock = LockService.getPublicLock();
    lock.waitLock(30000);  // wait 30 seconds before conceding defeat.

    try {
        var doc = SpreadsheetApp.openById("1obltJ3y8i4mHfRj2qz_WSR0gMHPh7ylR3gvbpn0qRwU");
        var commentSheet = doc.getSheetByName("comments");
        var urlSheet = doc.getSheetByName("urls");
        var userSheet = doc.getSheetByName("users");
        var dataSheet = doc.getSheetByName("server_data");

        var method = e.parameter.method;

        if ("addComment" == method) {
            // data 생성
            var headers = commentSheet.getRange(1, 1, 1, commentSheet.getLastColumn()).getValues()[0];
            var row = [];
            var paramData = JSON.parse(e.parameter.data);
            for (var i in headers) {
                if (headers[i] == "key") row.push("comment_"+paramData.id);
                if (headers[i] == "lastModifiedTime") row.push(new Date().getTime());
                if (headers[i] == "text") row.push(paramData.text);
                if (headers[i] == "originURL") row.push(paramData.originURL);
            }

            var targetRow = commentSheet.getLastRow()+1;
            commentSheet.getRange(targetRow, 1, 1, row.length).setValues([row]);

            addCommentIdToURL(paramData.id, paramData.originURL);

            return ContentService
                .createTextOutput(JSON.stringify({"result":"success"}))
                .setMimeType(ContentService.MimeType.JSON);
        }

        if ("editComment" == method) {
            var headers = commentSheet.getRange(1, 1, 1, commentSheet.getLastColumn()).getValues()[0];
            var row = [];
            var paramData = JSON.parse(e.parameter.data);
            for (var i in headers) {
                if (headers[i] == "key") row.push(paramData.id);
                if (headers[i] == "lastModifiedTime") row.push(new Date().getTime());
                if (headers[i] == "text") row.push(paramData.text);
                if (headers[i] == "originURL") row.push(paramData.originURL);
            }

            // search comment by id
            var targetRowIndex = getCommentRowIndexById(paramData.id);
            commentSheet.getRange(targetRowIndex, 1, 1, row.length).setValues([row]);

            // update url's last modified time
            var urlRowIndex = getUrlRowIndexByUrl(paramData.originURL);

            // edit data
            var column = 3; // column, last modified time
            var targetRow = urlSheet.getRange(urlRowIndex, column, 1, 1);
            targetRow.setValue(new Date().getTime());

            return ContentService
                .createTextOutput(JSON.stringify({"result":"success"}))
                .setMimeType(ContentService.MimeType.JSON);
        }

        if ("deleteComment" == method) {
            var paramData = JSON.parse(e.parameter.data);
            var targetRowIndex = getCommentRowIndexById(paramData.id);

            // delete from urls table
            var column = 3; // column, origin url
            var commentURL = commentSheet.getRange(targetRowIndex, column, 1, 1).getValue();
            deleteCommentIdToURL(paramData.id, commentURL);

            commentSheet.deleteRow(targetRowIndex);

            return ContentService
                .createTextOutput(JSON.stringify({"result":"success"}))
                .setMimeType(ContentService.MimeType.JSON);
        }

        if ("signin" == method) {
            var paramData = JSON.parse(e.parameter.data);
            if (userExistCheck(paramData.email)) {
                return ContentService
                    .createTextOutput(JSON.stringify({"result":"success"}))
                    .setMimeType(ContentService.MimeType.JSON);
            } else {
                return ContentService
                    .createTextOutput(JSON.stringify({"result":"error", "error": "user not found"}))
                    .setMimeType(ContentService.MimeType.JSON);
            }
        }

        if ("signup" == method) {
            var paramData = JSON.parse(e.parameter.data);
            if (userExistCheck(paramData.email)) {
                return ContentService
                    .createTextOutput(JSON.stringify({"result":"error", "error": "already exist user"}))
                    .setMimeType(ContentService.MimeType.JSON);
            } else {
                var headers = userSheet.getRange(1, 1, 1, userSheet.getLastColumn()).getValues()[0];
                var row = [];
                for (var i in headers) {
                    if (headers[i] == "email") row.push(paramData.email);
                    if (headers[i] == "pw") row.push(paramData.pw);
                    if (headers[i] == "validate") row.push(false);
                }

                var targetRow = userSheet.getLastRow()+1;
                userSheet.getRange(targetRow, 1, 1, row.length).setValues([row]);

                return ContentService
                    .createTextOutput(JSON.stringify({"result":"success"}))
                    .setMimeType(ContentService.MimeType.JSON);
            }
        }

        if ("mergeCache" == method) {
            var cachedData = JSON.parse(e.parameter.data);
            var cachedDataKeys = Object.getOwnPropertyNames(cachedData);

            cachedDataKeys.forEach(function(cachedDataKey) {
                var cachedDataValue = cachedData[cachedDataKey];

                // make data
                var headers = dataSheet.getRange(1, 1, 1, dataSheet.getLastColumn()).getValues()[0];
                var row = [];
                for (var i in headers) {
                    if (headers[i] == "id") row.push(cachedDataKey);
                    if (headers[i] == "text") row.push(cachedDataValue.text);
                    if (headers[i] == "lastmodifiedtime") row.push(cachedDataValue.lastmodifiedtime);
                }

                // search item
                var startRow = 2; // 1st is header row
                var column = 1; // 검색할 column, (id)
                var columnValues = dataSheet.getRange(startRow, column, dataSheet.getLastRow()).getValues();
                var searchResult = columnValues.findIndex(cachedDataKey);

                // write data to item
                var targetRow = ((searchResult != -1)) ? dataSheet.getRange(searchResult+2, 1, 1, row.length) : dataSheet.getRange(dataSheet.getLastRow()+1, 1, 1, row.length);
                targetRow.setValues([row]);

            });
            return ContentService
                .createTextOutput(JSON.stringify({"result":"success"}))
                .setMimeType(ContentService.MimeType.JSON);
        }

    } catch(e){
        // if error return this
        return ContentService
            .createTextOutput(JSON.stringify({"result":"error", "error": e}))
            .setMimeType(ContentService.MimeType.JSON);
    } finally { //release lock
        lock.releaseLock();
    }

}

function userExistCheck(email) {
    var doc = SpreadsheetApp.openById("1obltJ3y8i4mHfRj2qz_WSR0gMHPh7ylR3gvbpn0qRwU");
    var sheet = doc.getSheetByName("users");

    // search user by email
    var startRow = 2; // 1st is header row
    var column = 1; // 검색할 column, (email)
    var columnValues = sheet.getRange(startRow, column, sheet.getLastRow()).getValues();
    var searchResult = columnValues.findIndex(email);

    if (searchResult != -1) return true;
    else return false;
}

function addCommentIdToURL(newCommentId, url) {
    var doc = SpreadsheetApp.openById("1obltJ3y8i4mHfRj2qz_WSR0gMHPh7ylR3gvbpn0qRwU");
    var sheet = doc.getSheetByName("urls");

    // search url row
    var urlRowIndex = getUrlRowIndexByUrl(url);

    // edit data
    var column = 2; // column, comment ids
    var targetRow = sheet.getRange(urlRowIndex, column, 1, 1);
    var commentIds = JSON.parse(targetRow.getValue());
    commentIds.push(newCommentId);
    targetRow.setValue(JSON.stringify(commentIds));

    column = 3; // column, last modified time
    targetRow = sheet.getRange(urlRowIndex, column, 1, 1);
    targetRow.setValue(new Date().getTime());
}
function deleteCommentIdToURL(deleteCommentId, url) {
    var doc = SpreadsheetApp.openById("1obltJ3y8i4mHfRj2qz_WSR0gMHPh7ylR3gvbpn0qRwU");
    var sheet = doc.getSheetByName("urls");

    // search url row
    var urlRowIndex = getUrlRowIndexByUrl(url);

    // edit data
    var column = 2; // column, comment ids
    var targetRow = sheet.getRange(urlRowIndex, column, 1, 1);
    var commentIds = JSON.parse(targetRow.getValue());
    commentIds.remove(deleteCommentId.substr(8));
    targetRow.setValue(JSON.stringify(commentIds));

    column = 3; // column, last modified time
    targetRow = sheet.getRange(urlRowIndex, column, 1, 1);
    targetRow.setValue(new Date().getTime());
}

function getUrlRowIndexByUrl(url) {
    var doc = SpreadsheetApp.openById("1obltJ3y8i4mHfRj2qz_WSR0gMHPh7ylR3gvbpn0qRwU");
    var sheet = doc.getSheetByName("urls");

    var startRow = 2; // 1st is header row
    var column = 1; // 검색할 column, (url)
    var columnValues = sheet.getRange(startRow, column, sheet.getLastRow()).getValues();
    var searchResult = columnValues.findIndex("url_"+url);

    return searchResult+2;
}

function getCommentRowIndexById(commentId) {
    var doc = SpreadsheetApp.openById("1obltJ3y8i4mHfRj2qz_WSR0gMHPh7ylR3gvbpn0qRwU");
    var sheet = doc.getSheetByName("comments");

    var startRow = 2; // 1st is header row
    var column = 1; // 검색할 column, (comment id)
    var columnValues = sheet.getRange(startRow, column, sheet.getLastRow()).getValues();
    var searchResult = columnValues.findIndex(commentId);

    return searchResult+2;
}


function setup() {
    var doc = SpreadsheetApp.getActiveSpreadsheet();
    SCRIPT_PROP.setProperty("key", doc.getId());
}

Array.prototype.findIndex = function(search){
    if(search == "") return false;
    for (var i=0; i<this.length; i++)
        if (this[i] == search) return i;

    return -1;
};

Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

function generateId() {
    var timeStamp = new Date().getTime().toString();
    for (var i=0; i<10; i++) timeStamp += Math.floor((Math.random() * 10) + 1);
    return timeStamp;
}