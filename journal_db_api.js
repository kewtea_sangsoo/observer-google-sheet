var SCRIPT_PROP = PropertiesService.getScriptProperties(); // new property service

// If you don't want to expose either GET or POST methods you can comment out the appropriate function
function doGet(e){ return handleResponse(e); }
function doPost(e){ return handleResponse(e); }
function handleResponse(e) {
    // shortly after my original solution Google announced the LockService[1]
    // this prevents concurrent access overwritting data
    // [1] http://googleappsdeveloper.blogspot.co.uk/2011/10/concurrency-and-google-apps-script.html
    // we want a public lock, one that locks for all invocations
    var lock = LockService.getPublicLock();
    lock.waitLock(30000);  // wait 30 seconds before conceding defeat.

    try {
        var document = SpreadsheetApp.openById("1xpdMcO8UfbIitRAy-jeiFF7FVG1YQkKkiuqS2Psmbb8");

        var usersSheet = document.getSheetByName("users");
        var USERS_ID_COLUMNINDEX = 1;
        var USERS_EMAIL_COLUMNINDEX = 2;
        var USERS_PASSWORD_COLUMNINDEX = 3;
        var USERS_NAME_COLUMNINDEX = 4;
        var USERS_USERTYPE_COLUMNINDEX = 5;
        var USERS_MEMBERIDS_COLUMNINDEX = 6;
        var USERS_SERVICEIDS_COLUMNINDEX = 7;
        var USERS_CREATEDTIME_COLUMNINDEX = 8;
        var USERS_LASTMODIFIEDTIME_COLUMNINDEX = 9;
        var USERS_ACCOUNTSTATUS_COLUMNINDEX = 10;
        var USERS_AUTHOR_COLUMNINDEX = 11;

        var entitiesSheet = document.getSheetByName("entities");
        var ENTITIES_ID_COLUMNINDEX = 1;
        var ENTITIES_ENTITYTYPE_COLUMNINDEX = 2;
        var ENTITIES_AUTHOR_COLUMNINDEX = 3;
        var ENTITIES_CREATEDTIME_COLUMNINDEX = 4;
        var ENTITIES_LASTMODIFIEDTIME_COLUMNINDEX = 5;
        var ENTITIES_READACCESS_COLUMNINDEX = 6;
        var ENTITIES_WRITEACCESS_COLUMNINDEX = 7;
        var ENTITIES_COMMENTIDS_COLUMNINDEX = 8;
        var ENTITIES_TITLE_COLUMNINDEX = 9;
        var ENTITIES_URLS_COLUMNINDEX = 10;
        var ENTITIES_DATEFROM_COLUMNINDEX = 11;
        var ENTITIES_DATETO_COLUMNINDEX = 12;

        var commentsSheet = document.getSheetByName("comments");
        var COMMENTS_ID_COLUMNINDEX = 1;
        var COMMENTS_AUTHOR_COLUMNINDEX = 2;
        var COMMENTS_CREATEDTIME_COLUMNINDEX = 3;
        var COMMENTS_LASTMODIFIEDTIME_COLUMNINDEX = 4;
        var COMMENTS_READACCESS_COLUMNINDEX = 5;
        var COMMENTS_WRITEACCESS_COLUMNINDEX = 6;
        var COMMENTS_TEXT_COLUMNINDEX = 7;
        var COMMENTS_ORIGINURL_COLUMNINDEX = 8;
        var COMMENTS_COLLECTIONIDS_COLUMNINDEX = 9;
        var COMMENTS_KEYWORDIDS_COLUMNINDEX = 10;
        var COMMENTS_COMMENTTYPE_COLUMNINDEX = 11;
        var COMMENTS_QUOTE_COLUMNINDEX = 12;
        var COMMENTS_RANGES_COLUMNINDEX = 13;
        var COMMENTS_HIGHLIGHTS_COLUMNINDEX = 14;

        var collectionsSheet = document.getSheetByName("collections");
        var COLLECTIONS_ID_COLUMNINDEX = 1;
        var COLLECTIONS_AUTHOR_COLUMNINDEX = 2;
        var COLLECTIONS_CREATEDTIME_COLUMNINDEX = 3;
        var COLLECTIONS_LASTMODIFIEDTIME_COLUMNINDEX = 4;
        var COLLECTIONS_READACCESS_COLUMNINDEX = 5;
        var COLLECTIONS_WRITEACCESS_COLUMNINDEX = 6;
        var COLLECTIONS_COMMENTIDS_COLUMNINDEX = 7;
        var COLLECTIONS_NAME_COLUMNINDEX = 8;
        var COLLECTIONS_STATUS_COLUMNINDEX = 9;

        var keywordsSheet = document.getSheetByName("keywords");
        var KEYWORDS_ID_COLUMNINDEX = 1;
        var KEYWORDS_AUTHOR_COLUMNINDEX = 2;
        var KEYWORDS_CREATEDTIME_COLUMNINDEX = 3;
        var KEYWORDS_LASTMODIFIEDTIME_COLUMNINDEX = 4;
        var KEYWORDS_READACCESS_COLUMNINDEX = 5;
        var KEYWORDS_WRITEACCESS_COLUMNINDEX = 6;
        var KEYWORDS_COMMENTIDS_COLUMNINDEX = 7;
        var KEYWORDS_NAME_COLUMNINDEX = 8;

        var publicURLsSheet = document.getSheetByName("publicURLs");
        var PUBLICURLS_PUBLICURL_COLUMNINDEX = 1;
        var PUBLICURLS_COLLECTIONID_COLUMNINDEX = 2;

        var method = e.parameter.method;
        var paramData = JSON.parse(e.parameter.data);

        /** Users */

        if ("signUp" == method) {
            var userRowIndex = getRowIndexByQuery(usersSheet, USERS_EMAIL_COLUMNINDEX, paramData.email);
            if (userRowIndex) {
                var userStatusCell = getCellByRowColumn(usersSheet, userRowIndex, USERS_ACCOUNTSTATUS_COLUMNINDEX);
                throw {
                    isSuccess: false,
                    message: "User already signed in, ("+userStatusCell.getValue()+")"
                };
            } else {
                var currentTime = new Date().getTime();
                var headers = usersSheet.getRange(1, 1, 1, usersSheet.getLastColumn()).getValues()[0];
                var row = [];
                for (var i in headers) {
                    if ("id" == headers[i]) {
                        var userId = "uid_"+usersSheet.getLastRow();
                        row.push(userId);
                    }
                    if ("email" == headers[i]) row.push(paramData.email);
                    if ("password" == headers[i]) row.push(paramData.password);
                    if ("name" == headers[i]) row.push(paramData.name);
                    if ("userType" == headers[i]) row.push("user");
                    if ("memberIds" == headers[i]) row.push(null);
                    if ("serviceIds" == headers[i]) row.push("[\"journalUser\"]");
                    if ("createdTime" == headers[i]) row.push(currentTime);
                    if ("lastModifiedTime" == headers[i]) row.push(currentTime);
                    if ("accountStatus" == headers[i]) row.push("needVerify");
                    if ("author" == headers[i]) row.push(null);
                }
                usersSheet.getRange(usersSheet.getLastRow()+1, 1, 1, row.length).setValues([row]);
                throw { isSuccess: true };
            }
        }

        if ("signIn" == method) {
            var userRowIndex = getRowIndexByQuery(usersSheet, USERS_EMAIL_COLUMNINDEX, paramData.email);
            if (userRowIndex) {
                var userPw = getCellByRowColumn(usersSheet, userRowIndex, USERS_PASSWORD_COLUMNINDEX).getValue();
                if (paramData.password == userPw) {
                    var userStatus = getCellByRowColumn(usersSheet, userRowIndex, USERS_ACCOUNTSTATUS_COLUMNINDEX).getValue();
                    if ("verified" == userStatus) {
                        throw { isSuccess: true };
                    } else if ("signOut" == userStatus) {
                        throw {
                            isSuccess: false,
                            message: "Sign outed user."
                        };
                    } else {
                        throw {
                            isSuccess: false,
                            message: "User has not verified, please check verification email."
                        };
                    }
                } else {
                    throw {
                        isSuccess: false,
                        message: "Incorrect password."
                    };
                }
            } else {
                throw {
                    isSuccess: false,
                    message: "User not found."+userRowIndex
                };
            }
        }

        if ("signOut" == method) {
            var userRowIndex = getRowIndexByQuery(usersSheet, USERS_EMAIL_COLUMNINDEX, paramData.email);
            if (userRowIndex) {
                var userStatusCell = getCellByRowColumn(usersSheet, userRowIndex, USERS_ACCOUNTSTATUS_COLUMNINDEX);
                if ("signOut" == userStatusCell.getValue()) {
                    throw {
                        isSuccess: false,
                        message: "User already sign outed."
                    };
                } else {
                    userStatusCell.setValue("signOut");
                    throw { isSuccess: true };
                }
            } else {
                throw {
                    isSuccess: false,
                    message: "User not found."
                };
            }
        }

        /** ./Users */


        /** Comments */

        if ("addComment" == method || "updateComment" == method) {
            var currentTime = new Date().getTime();
            var headers = commentsSheet.getRange(1, 1, 1, commentsSheet.getLastColumn()).getValues()[0];
            var row = [];
            for (var i in headers) {
                if ("id" == headers[i]) row.push("comment_"+generateId());
                if ("author" == headers[i]) row.push(paramData.author);
                if ("createdTime" == headers[i]) row.push(currentTime);
                if ("lastModifiedTime" == headers[i]) row.push(currentTime);
                if ("readAccess" == headers[i]) row.push(paramData.readAccess);
                if ("writeAccess" == headers[i]) row.push(paramData.writeAccess);
                if ("text" == headers[i]) row.push(paramData.text);
                if ("originURL" == headers[i]) row.push(paramData.originURL);
                if ("collectionIds" == headers[i]) row.push(paramData.collectionIds);
                if ("keywordIds" == headers[i]) row.push(paramData.keywordIds);
                if ("commentType" == headers[i]) row.push(paramData.commentType);
                if ("quote" == headers[i]) row.push(paramData.quote);
                if ("ranges" == headers[i]) row.push(paramData.ranges);
                if ("highlights" == headers[i]) row.push(paramData.highlights);
            }

            // TODO: check and update: collections sheet, keywords sheet
            // 새로 추가된 collection: created collection, update comment id (add/delete)
            // 이미 있는  collection: update comment id (add/delete)
            // 새로 추가된 keyword: created keyword, update comment id (add/delete)
            // 이미 있는  keyword: update comment id (add/delete)

            if ("addComment" == method) {
                commentsSheet.getRange(commentsSheet.getLastRow()+1, 1, 1, row.length).setValues([row]);
                // TODO: update urls sheet: commentIds, lastModifiedTime
            }
            if ("updateComment" == method) {
                var targetRowIndex = getRowIndexByQuery(commentsSheet, COMMENTS_ID_COLUMNINDEX, paramData.id);
                commentsSheet.getRange(targetRowIndex, 1, 1, row.length).setValues([row]);
                // TODO: update urls sheet: lastModifiedTime
            }

            throw { isSuccess: true };
        }

        if ("deleteComment") {
            var targetRowIndex = getRowIndexByQuery(commentsSheet, COMMENTS_ID_COLUMNINDEX, paramData.id);
            commentsSheet.deleteRow(targetRowIndex);
            // TODO: update urls sheet: commentIds, lastModifiedTime
            // TODO: update collections sheet: commentIds, lastModifiedTime
            // TODO: update keyword sheet: commentIds, lastModifiedTime
        }

        /** ./Comments */


        /** Collections */

        // TODO: function 으로 분리하자, (addComment 등에서도 동일한 기능 실행 하므로)
        if ("addCollection" == method) {
            var currentTime = new Date().getTime();
            var headers = collectionsSheet.getRange(1, 1, 1, collectionsSheet.getLastColumn()).getValues()[0];
            var row = [];
            for (var i in headers) {
                if ("id" == headers[i]) row.push("collection_"+generateId());
                if ("author" == headers[i]) row.push(paramData.author);
                if ("createdTime" == headers[i]) row.push(currentTime);
                if ("lastModifiedTime" == headers[i]) row.push(currentTime);
                if ("readAccess" == headers[i]) row.push(paramData.readAccess);
                if ("writeAccess" == headers[i]) row.push(paramData.writeAccess);
                if ("commentIds" == headers[i]) row.push(null);
                if ("name" == headers[i]) row.push(paramData.name);
                if ("status" == headers[i]) row.push("normal");
            }
            collectionsSheet.getRange(collectionsSheet.getLastRow()+1, 1, 1, row.length).setValues([row]);

            throw { isSuccess: true };
        }

        // TODO: 무엇에 대한 update 인지 나눠서 처리하기? name 변경, commentIds 추가, commentIds 삭제
        if ("updateCollection" == method) {
            var currentTime = new Date().getTime();

            var targetRowIndex = getRowIndexByQuery(collectionsSheet, COLLECTIONS_ID_COLUMNINDEX, paramData.collectionId);

            var nameCell = getCellByRowColumn(collectionsSheet, targetRowIndex, COLLECTIONS_NAME_COLUMNINDEX);
            nameCell.setValue(paramData.name);

            // ...
        }

        /** ./Collections */


        /** Keywords */

        // collections api 구성 다되면, 단어 몇개만 바꾸면 비슷한 코드들 일듯.

        /** ./Keywords */


        /** PublicURLs */

        /** ./PublicURLs */


    } catch(event) {

        if (event.isSuccess) {
            return ContentService
                .createTextOutput(JSON.stringify({"result":"success"}))
                .setMimeType(ContentService.MimeType.JSON);
        } else {
            return ContentService
                .createTextOutput(JSON.stringify({"result":"error", "error": event.message}))
                .setMimeType(ContentService.MimeType.JSON);
        }


    } finally { //release lock
        lock.releaseLock();
    }

}

/** Support functions */
function getRowIndexByQuery(sheet, columnIndex, query) {
    var columnValues = sheet.getRange(2, columnIndex, sheet.getLastRow()).getValues();
    var searchResult = columnValues.findIndex(query);
    if (-1 == searchResult) return false;
    else return searchResult+2;
}
function getCellByRowColumn(sheet, rowIndex, columnIndex) {
    return sheet.getRange(rowIndex, columnIndex, 1, 1);
}

function generateId() {
    var timeStamp = new Date().getTime().toString();
    for (var i=0; i<10; i++) timeStamp += Math.floor((Math.random() * 10) + 1);
    return timeStamp;
}

//

function setup() {
    var doc = SpreadsheetApp.getActiveSpreadsheet();
    SCRIPT_PROP.setProperty("key", doc.getId());
}

Array.prototype.findIndex = function(search){
    if(search == "") return false;
    for (var i=0; i<this.length; i++)
        if (this[i] == search) return i;

    return -1;
};

Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};